**Как использовать**

>Для каждого использования на данном этапе необходимо полностью перезагружать приложение (из Xcode или из памяти) на обоих устройствах.
>Устройства могут передавать данные между собой и симуляторами, симуляторы могут тоже передавать данные между собой.
>
>Устройства должны находиться в одной Wi-Fi сети (но это не точно, с двух устройств не тестировали)

**Известные проблемы**

>В списках присутствуют дубликаты устройств
>При повторном использовании одно из приложений не гарантирована работа передатчика

