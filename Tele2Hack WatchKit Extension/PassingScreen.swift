//
//  PassingScreen.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI
import Network

enum typesOfData {
    case mobile
    case internet
}

struct AmoutScreen: View {
    var receiver: NWBrowser.Result
    @State private var typeOfData = typesOfData.internet
    @State private var showingResult = false
    @State private var alertTitle = "Success"
    
    var objectToPass : String {
        switch typeOfData {
        case .mobile:
            return "Min"
        case .internet:
            return "Gb"
        }
    }
    
    @State private var amount = 1.0
    
    var body: some View {
        VStack {
            
            HStack {
                Text("To:")
                Text(getDeviceName(of: receiver))
            }
            .offset(y: 10)
            HStack {
                Button (action: { if self.typeOfData == typesOfData.mobile {self.typeOfData = typesOfData.internet} else {self.typeOfData = typesOfData.mobile}})
                {
                    Image(systemName: "chevron.left")
                    .frame(width: 20)
                    
                }
                .buttonStyle(PlainButtonStyle())
                ZStack {
                    Circle()
                        
                    HStack {
                        Text("\(amount, specifier: "%.2f")")
                        .foregroundColor(Color.black)
                        .focusable(true)
                        .digitalCrownRotation($amount, from: 0, through: 10, by: 0.5, sensitivity: .low, isContinuous: false, isHapticFeedbackEnabled: true)
                        Text(objectToPass)
                        .foregroundColor(Color.black)
                    }
                }
                .frame(height: 85)
                Button (action: { if self.typeOfData == typesOfData.mobile {self.typeOfData = typesOfData.internet} else {self.typeOfData = typesOfData.mobile}}) {Image(systemName: "chevron.right")}
                .buttonStyle(PlainButtonStyle())
            }
            Button(action: { self.showingResult = true }) { Text("Send")}
        }
        .navigationBarTitle(Text("Back"))
        .alert(isPresented: $showingResult) {
            Alert(title: Text(alertTitle))
        }
    
        
    }
}

//struct PassingScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        PassingScreen(receiver: "Person")
//    }
//}



