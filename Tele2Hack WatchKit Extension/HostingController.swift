//
//  HostingController.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
