//
//  PassingScreen.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI
import Network

struct AmoutView: View {
    var receiver: NWBrowser.Result
    private let sender = Sender()
    @State private var dataType = DataType.internet
    @State private var showingResult = false
    @State private var alertTitle = "Отправлено"
    
    @State private var amount = 1.0
    
    var body: some View {
        VStack {
            HStack {
                Text(">")
                    .font(.custom("SF Compact Rounded Regular", size: 12))
                Text(getDeviceName(of: receiver))
            }
            .offset(y: 10)

            HStack {
                HStack {
                    Text("\(amount, specifier: "%.2f")")
                    .font(Font.custom("SF Compact Rounded Bold", size: 23))
                    .onTapGesture {
                            if self.dataType == DataType.mobile {self.dataType = DataType.internet} else {self.dataType = DataType.mobile}
                    }
                    .focusable(true)
                    .digitalCrownRotation($amount, from: 0, through: 50, by: 1, sensitivity: .low, isContinuous: false, isHapticFeedbackEnabled: true)
                    Text(self.dataType.rawValue)
                    .font(Font.custom("SF Compact Rounded Regular", size: 20))
                    .onTapGesture {
                            if self.dataType == DataType.mobile {self.dataType = DataType.internet} else {self.dataType = DataType.mobile}
                    }
                    .frame(height: 85)
                }
            }
            Button(action: {
                sharedConnection?.sendMessage("Абонент \(deviceName) отправил вам \(self.amount) \(self.dataType.rawValue)")
                self.showingResult = true
            }) {
                Text("Отправить")
            }
        }
        .onAppear(perform: {
            self.sender.connect(to: self.receiver)
        })
        .alert(isPresented: $showingResult) {
            Alert(title: Text(alertTitle), message: nil, dismissButton: .default(Text("OK"), action: {
                clear()
            }))
        }
    }
}



