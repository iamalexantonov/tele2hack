//
//  SendView.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI

struct BrowserView: View {
    @ObservedObject var sender = BrowserVM()
    @State var amount: Int = 0
    
    var body: some View {
        VStack(alignment: .leading) {
            Rectangle()
                .frame(width: 20.0, height: 15.0)
                .foregroundColor(Color.black)
            List {
                ForEach(sender.results, id: \.self) { result in
                    NavigationLink(destination: AmoutView(receiver: result)) {
                        NameCell(person: getDeviceName(of: result))
                        .frame(height: 80.0)
                        .shadow(radius: 20)
                }
                .listRowPlatterColor(Color.green)
            }
        }.listStyle(CarouselListStyle())
    }
}
}
    
struct NameCell: View {
    var person: String
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(person)
                .font(.system(size: 18, design: .rounded))
            }
        }
        .padding()
    }
}

struct SendView_Previews: PreviewProvider {
    static var previews: some View {
        BrowserView()
    }
}
