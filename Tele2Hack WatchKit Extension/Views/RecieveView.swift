//
//  RecieveView.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI

struct RecieveView: View {
    @State private var opacity = 1.0
    @ObservedObject var reciever: RecieverVM
    @State private var moveOn = true
    
    var foreverAnimation: Animation {
        Animation.easeInOut(duration: 0.3)
           .repeatForever()
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                Circle()
                    .opacity(self.moveOn ? 0.8 : 0.5)
                    .scaleEffect(self.moveOn ? 1 : 1.1)
                .frame(width: geometry.size.width - 20, height: geometry.size.height - 20)
                .foregroundColor(Color.green)
                .animation(Animation.linear(duration: 2).repeatForever(autoreverses: true))
                .onAppear(){ self.moveOn.toggle() }
                Text("Получение")
                .font(Font.custom("SF Compact Rounded Regular", size: 20))
                
            }
        }.onAppear(perform: {
            withAnimation(self.foreverAnimation) { self.opacity = 0.7 }
            self.reciever.setUp()
        })
        .onDisappear(perform: {
            sharedConnection?.cancel()
            sharedConnection = nil
        })
        .alert(isPresented: $reciever.messageRecieved, content: {
            Alert(title: Text(self.reciever.message), message: nil, dismissButton: Alert.Button.default(Text("OK"), action: {
                clear()
            }))
        })
    }
}

struct RecieveView_Previews: PreviewProvider {
    static var previews: some View {
        RecieveView(reciever: RecieverVM())
    }
}
