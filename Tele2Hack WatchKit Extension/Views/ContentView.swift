//
//  ContentView.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            ZStack {
                Image("TELEPASSW")
                .resizable()
                .scaledToFit()
            }
                .frame(width: 70, height: 20)
                
                Spacer()
            NavigationLink(destination: BrowserView().navigationBarTitle("Tele2"), label: {
                Text("Отправить").font(.headline)
            }).padding(.vertical)
            NavigationLink(destination: RecieveView(reciever: RecieverVM()).navigationBarTitle("Tele2"), label: {
                Text("Получить").font(.headline)
            }).padding(.vertical)

        }.onAppear(perform: {
            clear()
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
