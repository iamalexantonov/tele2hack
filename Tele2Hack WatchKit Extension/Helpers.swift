//
//  Helpers.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import Foundation
import Network
import WatchKit

let deviceName = WKInterfaceDevice.current().name

func getDeviceName(of: NWBrowser.Result) -> String {
    if case let NWEndpoint.service(name: name, type: _, domain: _, interface: _) = of.endpoint {
        return name
    } else {
        return "Неизвестная зверушка"
    }
}

enum DataType:String {
    case mobile = "Мин"
    case internet = "Гб"
}

func clear(){
    sharedConnection?.cancel()
    sharedConnection = nil
    sharedListener = nil
    sharedBrowser = nil
}
