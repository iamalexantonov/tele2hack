//
//  SenderVM.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import Foundation
import Network
import WatchKit

final class BrowserVM:ObservableObject {
    @Published var results: [NWBrowser.Result] = [NWBrowser.Result]()
    
    init() {
        sharedBrowser = PeerBrowser(delegate: self)
    }
    
    func send(to: NWBrowser.Result, message: String) {
        if sharedConnection == nil {
            sharedConnection = PeerConnection(endpoint: to.endpoint,
            interface: to.interfaces.first,
            passcode: "0000",
            delegate: self)
        }
    }
}

extension BrowserVM: PeerConnectionDelegate {
    func connectionReady() {
        print("Connected")
    }
    
    func connectionFailed() {
        print("Ой")
    }
    
    func receivedMessage(content: Data?, message: NWProtocolFramer.Message) {
        //sharedConnection?.cancel()
    }
    
    func displayAdvertiseError(_ error: NWError) {
        print(error.localizedDescription)
    }
    
}

extension BrowserVM: PeerBrowserDelegate {
    func refreshResults(results: Set<NWBrowser.Result>) {
        self.results = [NWBrowser.Result]()
        for result in results {
            if case let NWEndpoint.service(name: name, type: _, domain: _, interface: _) = result.endpoint {
                if name != deviceName {
                self.results.append(result)
                }
            }
        }
    }
    
    func displayBrowseError(_ error: NWError) {
        print(error.localizedDescription)
    }
    
    
}
