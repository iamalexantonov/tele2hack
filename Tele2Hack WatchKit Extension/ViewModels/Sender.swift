//
//  Sender.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import Foundation
import Network

class Sender {
    func connect(to: NWBrowser.Result) {
        if sharedConnection == nil {
            sharedConnection = PeerConnection(endpoint: to.endpoint,
                                              interface: to.interfaces.first,
                                              passcode: "0000",
                                              delegate: self)
        }
    }
}

extension Sender: PeerConnectionDelegate {
    func connectionReady() {
        print("Connected")
    }
    
    func connectionFailed() {
        print("Ой")
    }
    
    func receivedMessage(content: Data?, message: NWProtocolFramer.Message) {
        //sharedConnection?.cancel()
    }
    
    func displayAdvertiseError(_ error: NWError) {
        print(error.localizedDescription)
    }
    
}
