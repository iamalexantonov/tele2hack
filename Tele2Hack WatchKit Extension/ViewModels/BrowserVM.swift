//
//  SenderVM.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import Foundation
import Network

final class BrowserVM:ObservableObject {
    @Published var results: [NWBrowser.Result] = [NWBrowser.Result]()
    
    init() {
        sharedBrowser = PeerBrowser(delegate: self)
    }
}


extension BrowserVM: PeerBrowserDelegate {
    func refreshResults(results: Set<NWBrowser.Result>) {
        self.results = [NWBrowser.Result]()
        for result in results {
            if case let NWEndpoint.service(name: name, type: _, domain: _, interface: _) = result.endpoint {
                if name != deviceName {
                self.results.append(result)
                }
            }
        }
    }
    
    func displayBrowseError(_ error: NWError) {
        print(error.localizedDescription)
    }
    
}
