//
//  RecieverVM.swift
//  Tele2Hack WatchKit Extension
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import Foundation
import Network

final class RecieverVM: ObservableObject {
    @Published var message = "" {
        didSet {
            messageRecieved = true
        }
    }
    @Published var messageRecieved = false
    
    func setUp() {
        sharedListener = PeerListener(name: deviceName, passcode: "0000", delegate: self)
    }
}

extension RecieverVM: PeerConnectionDelegate {
    func connectionReady() {
        print("Законнектились")
    }
    
    func connectionFailed() {
        print("Ошибочка вышла")
    }
    
    func receivedMessage(content: Data?, message: NWProtocolFramer.Message) {
        if let content = content, let messageString = String(data: content, encoding: .unicode) {
            self.message = messageString
        }
    }
    
    func displayAdvertiseError(_ error: NWError) {
        print(error.localizedDescription)
    }
}
